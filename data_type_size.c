// LP64: 64bit UNIX
// LLP64: 64bit Windows

//            �z  32bit  �{  �z     64bit      �{
// TYPE       LP32  ILP32  LP64  ILP64  LLP64

// char         8     8      8      8      8
// short       16    16     16     16     16
// int         16    32     32     64     32
// long        32    32     64     64     32
// long long   64    64     64     64     64
// pointer     32    32     64     64     64 

// LP32:   32bit long, pointer
// ILP32*: 32bit int, long, pointer
// LP64*:  64bit long, pointer
// ILP64:  64bit int, long, pointer
// LLP64:  64bit long long, pointer

#include <stdio.h>

int main()
{
    printf("sizeof char = %lu\n", sizeof(char));
    printf("sizeof short = %lu\n", sizeof(short));
    printf("sizeof int = %lu\n", sizeof(int));
    printf("sizeof long = %lu\n", sizeof(long));
    printf("sizeof long long = %lu\n", sizeof(long));
    printf("sizeof pointer = %lu\n", sizeof((void*)0));
    printf("sizeof float = %lu\n", sizeof(float));
    printf("sizeof double = %lu\n", sizeof(double));
    printf("sizeof long double = %lu\n", sizeof(long double));
    
    return 0;
}