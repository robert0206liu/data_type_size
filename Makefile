all: data_type_size.c
	gcc data_type_size.c -o data_type_size

debug:
	gcc -g data_type_size.c -o data_type_size

clean:
	rm data_type_size
